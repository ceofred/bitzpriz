import React from 'react';
import Fetch from 'isomorphic-unfetch';
import Layout from '../components/Layout';

import Prices from '../components/Prices';

const indexPage = (props) => {   
    return(
        <Layout title="Home page">
        <div>
<h1>Home</h1>
<Prices bpi={props.bpi} />
        </div>
        </Layout>
         
    );
}
indexPage.getInitialProps = async function(){
    const res = await fetch('https://api.coindesk.com/v1/bpi/currentprice.json');
    const data = await res.json();
    return {
        bpi:data
    }
}

export default indexPage;
