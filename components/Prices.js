
class Prices extends React.Component{
    state = {
        currency:"USD"
    }

    render(){

        let list = '';
        if(this.state.currency === 'USD'){
            list = ( <li className="list-group-item">
            Bitcoin rate for {this.props.bpi.bpi.USD.description} :
            <span className="badge badge-success">{this.props.bpi.bpi.USD.code}</span>
            {this.props.bpi.bpi.USD.rate}
        </li>
       );
        }else if(this.state.currency === 'GBP'){
            list = ( <li className="list-group-item">
            Bitcoin rate for {this.props.bpi.bpi.GBP.description} :
            <span className="badge badge-success">{this.props.bpi.bpi.GBP.code}</span>
            {this.props.bpi.bpi.GBP.rate}
        </li>);
        }else if(this.state.currency === 'EUR'){
            list = ( <li className="list-group-item">
            Bitcoin rate for {this.props.bpi.bpi.EUR.description} :
            <span className="badge badge-success">{this.props.bpi.bpi.EUR.code}</span>
            {this.props.bpi.bpi.EUR.rate}
        </li>);
        }
        return(
            <div>
                    {this.props.bpi.disclaimer}

                <ul className="list-group">
                    {list}
                </ul>
                <br />
                <select onChange={e => this.setState({currency: e.target.value})}>
<option value="USD">USD</option>
<option value="EUR">EUR</option>
<option value="GBP">GBP</option>

                </select>
            </div>  
        );
    }
}

export default Prices;