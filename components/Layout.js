import Navbar from './Navbar';
import Head from 'next/head';

const Layout = (props) => {
return( <div>
    <Head>
        <title>{props.title}</title>
        <link href="https://bootswatch.com/4/flatly/bootstrap.min.css" rel="stylesheet"/>
    </Head>
<Navbar />
<div className="container">
{props.children}

</div>

</div>);
}

export default Layout;